﻿module AccountingBot

open Funogram.Api
open Funogram.Telegram
open Funogram.Telegram.Bot
open Funogram.Telegram.Types
open Argu
open System
open System.Collections.Specialized
open Microsoft.Extensions.Caching.Memory
open Microsoft.Extensions.Options
open Microsoft.Extensions.Caching.Memory

module AccountTypeNames =
    [<Literal>]
    let Sevenergosbyt = "Севэнергосбыт"

    [<Literal>]
    let Sevgas = "Севастопольгаз"

    [<Literal>]
    let Sevvodokanal = "Водоканал"

type AccountType =
    | Sevenergosbyt
    | Sevgas
    | Sevvodokanal

type ReportMetersData =
    { AccountId: Guid
      MeterReading: decimal }

type AccountCreationData =
    { Name: string option
      Type: AccountType option
      Login: string option
      Password: string option }

type ChatState =
    | ReportMeters of ReportMetersData
    | AccountCreation of AccountCreationData
    | Menu

type MessageHandler = Message -> UpdateContext -> ChatHandlerState -> ChatHandlerState

and ChatHandlerState =
    { MessageHandler: MessageHandler
      State: ChatState }

module CommandNames =
    [<Literal>]
    let DefaultStart = "/start"

    [<Literal>]
    let ReportMeters = "Передать показания"

    [<Literal>]
    let Settings = "Настройки"

    [<Literal>]
    let AddAccount = "Добавить счёт"

    [<Literal>]
    let RemoveAccount = "Удалить счёт"

let buildKeyboard (buttonsScheme: string[][]) =
    ReplyKeyboardMarkup(ReplyKeyboardMarkup.Create(buttonsScheme |> Array.map (Array.map KeyboardButton.Create)))

let MainMenuKeyboard =
    [| [| CommandNames.ReportMeters |]; [| CommandNames.Settings |] |]
    |> buildKeyboard

let SettingsMenuKeyboard =
    [| [| CommandNames.AddAccount |]; [| CommandNames.RemoveAccount |] |]
    |> buildKeyboard

let AccountTypeKeyboard =
    [| [| AccountTypeNames.Sevenergosbyt |]
       [| AccountTypeNames.Sevgas |]
       [| AccountTypeNames.Sevvodokanal |] |]
    |> buildKeyboard

let RemoveKeyboardMarkup =
    ReplyKeyboardRemove(
        { ReplyKeyboardRemove.RemoveKeyboard = true
          Selective = None }
    )

let (|Prefix|_|) (p: string) (s: string) =
    if s.StartsWith(p) then
        Some(s.Substring(p.Length))
    else
        None

let (|PlainCommand|_|) (p: string) (s: string) =
    if s.StartsWith(p) then Some(s, true) else None

let syncApiRequest (ctx: UpdateContext) request =
    request |> api ctx.Config |> Async.Ignore |> Async.Start

let CommandHandler (handler: Message -> UpdateContext -> ChatHandlerState -> ChatHandlerState) (ctx: UpdateContext) =
    printf "CommandHandler %A" handler
    handler (Option.get ctx.Update.Message) ctx

let MainMenu (msg: Message) (ctx: UpdateContext) (state: ChatHandlerState) : ChatHandlerState =
    Api.sendMessageMarkup msg.Chat.Id "Выберите операцию" MainMenuKeyboard
    |> syncApiRequest ctx

    state

let SettingsMenu (msg: Message) (ctx: UpdateContext) (state: ChatHandlerState) : ChatHandlerState =
    Api.sendMessageMarkup msg.Chat.Id "Выберите операцию" SettingsMenuKeyboard
    |> syncApiRequest ctx

    state

let HandleAccountTypeResponse (response: string) (state: ChatState) : Result<ChatState, string> =
    match state with
      | AccountCreation data ->
          let accountType =
            match response with
            | AccountTypeNames.Sevenergosbyt -> Some AccountType.Sevenergosbyt
            | AccountTypeNames.Sevgas -> Some AccountType.Sevgas
            | AccountTypeNames.Sevvodokanal -> Some AccountType.Sevvodokanal
            | _ -> None

          if Option.isSome accountType then
              Ok(ChatState.AccountCreation { data with Type = accountType })
          else
              Error "Неправильный тип счёта."
      | _ -> Error "Неправильное состояние."

let HandleAccountName (response: string) (state: ChatState) : Result<ChatState, string> =
    match state with
    | AccountCreation data -> Ok(AccountCreation { data with Name = Some response })
    | _ -> Error "Неправильное состояние."

let HandleAccountLogin (response: string) (state: ChatState) : Result<ChatState, string> =
    match state with
    | AccountCreation data -> Ok(AccountCreation { data with Login = Some response })
    | _ -> Error "Неправильное состояние."

let HandleAccountPassword (response: string) (state: ChatState) : Result<ChatState, string> =
    match state with
    | AccountCreation data -> Ok(AccountCreation { data with Password = Some response })
    | _ -> Error "Неправильное состояние."

type ResponseHandler = string -> ChatState -> Result<ChatState, string>

type InputRequest =
    { Text: string
      Markup: Markup
      Handler: ResponseHandler
      RetriesLeft: int }

    static member Create text markup handler =
        { Text = text
          Markup = markup
          Handler = handler
          RetriesLeft = 0 }

let rec InputHandler
    (resultHandler: ChatState -> ChatHandlerState)
    (request: InputRequest)
    (requests: InputRequest list)
    (msg: Message)
    (ctx: UpdateContext)
    (state: ChatHandlerState)
    : ChatHandlerState =
    let result = request.Handler (Option.defaultValue "" msg.Text) state.State

    match result with
    | Ok newState ->
        if not requests.IsEmpty then
            let nextRequest = requests.Head

            Api.sendMessageMarkup msg.Chat.Id nextRequest.Text nextRequest.Markup
            |> syncApiRequest ctx

            { state with
                MessageHandler = (InputHandler resultHandler nextRequest (List.tail requests))
                State = newState }
        else
            resultHandler newState
    | Error errorMessage ->
        if request.RetriesLeft > 0 then
            Api.sendMessageMarkup msg.Chat.Id ("Ошибка ввода: " + errorMessage + "\n" + request.Text) request.Markup
            |> syncApiRequest ctx

            { state with
                MessageHandler =
                    InputHandler
                        resultHandler
                        { request with
                            RetriesLeft = request.RetriesLeft - 1 }
                        requests }
        else
            Api.sendMessage msg.Chat.Id ("Ошибка: " + errorMessage) |> syncApiRequest ctx
            resultHandler state.State

let rec PrintAccount (chatState: ChatState) : ChatHandlerState =
    printfn "%A" chatState

    { MessageHandler = MenuHandler
      State = Menu }

and AddAccountHandler (msg: Message) (ctx: UpdateContext) (state: ChatHandlerState) : ChatHandlerState =
    let text = "Введите название счёта"
    Api.sendMessageMarkup msg.Chat.Id text RemoveKeyboardMarkup |> syncApiRequest ctx

    { state with
        MessageHandler =
            InputHandler
                PrintAccount
                (InputRequest.Create text RemoveKeyboardMarkup HandleAccountName)
                [ ({ Text = "Выберите тип счёта"
                     Markup = AccountTypeKeyboard
                     Handler = HandleAccountTypeResponse
                     RetriesLeft = 1 })
                  (InputRequest.Create "Введите логин" RemoveKeyboardMarkup HandleAccountLogin)
                  (InputRequest.Create "Введите пароль" RemoveKeyboardMarkup HandleAccountPassword) ] 
        State =
          (AccountCreation
            { Name = None
              Type = None
              Login = None
              Password = None })}

and MenuHandler (msg: Message) (ctx: UpdateContext) (state: ChatHandlerState) : ChatHandlerState =
    let text = Option.defaultValue "" msg.Text

    match text with
    | PlainCommand CommandNames.DefaultStart _ -> CommandHandler MainMenu ctx state
    | PlainCommand CommandNames.Settings _ -> CommandHandler SettingsMenu ctx state
    | PlainCommand CommandNames.AddAccount _ -> CommandHandler AddAccountHandler ctx state
    | _ ->
        printfn "Not matched command in %A" text
        state

let Fallback (msg: Message) (ctx: UpdateContext) : unit =
    printfn "Not matched command in %A" msg.Text

let updateArrived (state: MemoryCache) (ctx: UpdateContext) =
    printfn "%A" (Option.map (fun (msg: Message) -> Option.orElse (Some "") msg.Text) ctx.Update.Message)

    ctx.Update.Message
    |> Option.map (fun msg ->
        let msg = Option.get ctx.Update.Message
        let id = msg.Chat.Id

        printfn "%A" id

        let chatState =
            state.GetOrCreate(
                string id,
                (fun cacheEntry ->
                    cacheEntry.SlidingExpiration <- TimeSpan.FromHours 1

                    { MessageHandler = MenuHandler
                      State = Menu })
            )

        let newChatState = chatState.MessageHandler msg ctx chatState
        state.Set(string id, newChatState) |> ignore)
    |> ignore

    printfn "%A" (state.Count)



type CliArguments =
    | [<Unique>] Token of string

    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | Token _ -> "telegram bot token"

[<EntryPoint>]
let main args =
    let parser = ArgumentParser.Create<CliArguments>()
    let arguments = parser.Parse args

    let config =
        Config.defaultConfig
        |> (fun config ->
            if arguments.Contains Token then
                { config with
                    Token = arguments.GetResult Token }
            else
                config |> Config.withReadTokenFromEnv "TELEGRAM_TOKEN")

    async {
        let! _ = Api.deleteWebhookBase () |> api config

        let state =
            new MemoryCache(new OptionsWrapper<MemoryCacheOptions>(new MemoryCacheOptions()))

        return! startBot config (updateArrived state) None
    }
    |> Async.RunSynchronously

    0
